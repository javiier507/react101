import React, { Component } from 'react';
import Tarjeta from './Tarjeta';
import './styles.css';

export default class Lista extends Component {
    constructor(props) {
        super(props)

        this.state = {
            tarjetas : ['carlos', 'javier', 'amber']
        }

        this.eliminar = this.eliminar.bind(this);
        this.actualizar = this.actualizar.bind(this);
    }

    iterar(item, i) {
        return (
            <Tarjeta key={i} index={i} nombre={item}  onEliminar={this.eliminar} onActualizar={this.actualizar}>
                {i+1}
            </Tarjeta>
        )
    }

    agregar() {
        var nueva = this.refs.nueva.value;

        if(nueva === "") {
            nueva = "nueva tarjeta";
        }

        var arreglo = this.state.tarjetas;
        arreglo.push(nueva);
        this.setState({ tarjetas : arreglo});
        this.refs.nueva.value = "";
    }

    agregarEnter(e) {
        if(e.charCode === 13) {
            this.agregar();
        }
    }

    eliminar(i) {
        var arreglo = this.state.tarjetas;
        arreglo.splice(i, 1);
        this.setState({ tarjetas : arreglo});
    }

    actualizar(nombre, index) {
        var arreglo = this.state.tarjetas;
        arreglo[index] = nombre;
        this.setState({ tarjetas : arreglo});
    }

    render() {
        return (
            <div className="centerBlock">
                <header>
                    <h1>Tarjetas Favoritas</h1>
                    <i>Total : {this.state.tarjetas.length}</i>
                </header>
                <div className="input-group">
                    <input type="text" ref="nueva" onKeyPress={this.agregarEnter.bind(this)} className="form-control" placeholder="Agregar Nueva Tarjeta" />
                    <span className="input-group-btn">
                        <div onClick={this.agregar.bind(this)} className="btn btn-default btn-success">
                            +
                        </div>
                    </span>
                </div>
                <div>
                    {
                        this.state.tarjetas.map(this.iterar.bind(this))
                    }
                </div>
            </div>
        )
    }
}
