import React, { Component } from 'react';

export default class Tarjeta extends Component {

    constructor(props) {
        super(props)

        this.state = {
            meGusta: true,
            editado: false
        }
    }

    clickear() {
        this.setState({ meGusta : !this.state.meGusta })
    }

    eliminar() {
        this.props.onEliminar(this.props.index);
    }

    editar() {
        this.setState({ editado : true })
    }

    cancelar() {
        this.setState({ editado : false })
    }

    guadar() {
        this.props.onActualizar(this.refs.nuevoNombre.value, this.props.index);
        this.setState({ editado : false })
    }

    mostrarVistaEditar() {
        return (
            <div className="comida">
                <input
                    type="text"
                    ref="nuevoNombre"
                    defaultValue={this.props.nombre}
                    className="form-control"
                    placeholder="Nuevo Nombre" />
                <div>
                    <div className="glyphicon glyphicon-ok-circle blue" onClick={this.guadar.bind(this)}></div>
                    <div className="glyphicon glyphicon-remove-circle red" onClick={this.cancelar.bind(this)}></div>
                </div>
            </div>
        );
    }

    mostrarVistaFinal() {
        return (
            <div className="comida">
                <h1 className="bg-success">{this.props.nombre}</h1>
                <p className="bg-info">
                    Número : <i>{this.props.children}</i>
                </p>
                <div>
                    <input
                        onChange={this.clickear.bind(this)}
                        defaultChecked={this.state.meGusta}
                        type="checkbox"
                        className="glyphicon glyphicon-heart glyphicon-heart-lg" />
                    <br/>
                    Like : {String(this.state.meGusta)}
                </div>
                <div>
                    <div className="glyphicon glyphicon-pencil blue" onClick={this.editar.bind(this)}></div>
                    <div className="glyphicon glyphicon-trash red" onClick={this.eliminar.bind(this)}></div>
                </div>
            </div>
        );
    }

    render() {
        if(this.state.editado) {
            return this.mostrarVistaEditar();
        }
        else {
            return this.mostrarVistaFinal();
        }
    }
}
